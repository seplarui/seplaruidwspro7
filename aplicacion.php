<?php
include_once 'seguridad.php';
?>

<html>
    <head>
        <meta charset="utf-8">
        <title>Gestión de Vehículos</title>
    </head>
    <div align="center">
    <h1>Gestión de Vehículos</h1>
    <hr>
    <p>Proyecto 7. LOGIN</p>
    <p>Elegir:</p>
    <lu><li><a href="Marca_Controlador.php">Gestión de la Marca del Vehículo</a></li></lu>
    <lu><li><a href="ModeloV_Controlador.php">Gestión del Modelo del Vehículo</a></li></lu>
    <lu><li><a href="Instalar.php">Instalación BDA.</a></li></lu>
    
    <p>Documentación por tema:</p>
    <lu><li><a href="GESTION_DE_VEHICULOS.pdf">Tema 5. POO. Enunciado</a></li></lu>
    <lu><li><a href="GESTION_DE_VEHICULOS_MYSQL.pdf">Tema 6. MYSQL. Enunciado</a></li></lu>
    
    <p>Documentación de este proyecto:</p>
    <lu><li><a href="DIAGRAMA_DE_RELACIONES_MYSQL.pdf">Documentación</a></li></lu>
    <p>&copy; <?php include_once ("config.php");
    echo config::$autor." ".config::$fecha." "." ".config::$empresa." ".config::$curso ?>
    </p>
    <hr>
    
    </div>
</html>
